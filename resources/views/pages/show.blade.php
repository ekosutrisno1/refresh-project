@extends('layouts.app')

@section('judul')
{{$judul}}
@endsection

@section('content')
<div class="container">
   <div class="card mx-auto">
      <h5 class="card-header">Detail Biodata | <span class="badge badge-success">{{$biodata->no_identitas}}</span>
      </h5>
      <div class="row p-4">
         <div class="col-md-4">
            <div class="card-body">
               <h5 class="card-title"><i class="fa fa-fw fa-user"></i> {{$biodata->nama_lengkap}}</h5>
               <div class="card" style="width: 18rem;">
                  <ul class="list-group list-group-flush">
                     <li class="list-group-item">Email: {{$biodata->email}}</li>
                     <li class="list-group-item">Gender:
                        @if ($biodata->gender==1)
                        <span>Laki-laki</span>
                        @else
                        <span>Perempuan</span>
                        @endif
                     </li>
                     <li class="list-group-item">Umur: {{$biodata->umur}} Tahun</li>
                     <li class="list-group-item bg-primary text-white font-weight-bold">Posisi Sekarang: <br>
                        {{$biodata->posisi_sekarang}}
                     </li>
                  </ul>
               </div>
               <a href="/all" class="btn btn-primary mt-5"><i class="fa fa-fw fa-arrow-left"></i> Back to home</a>
            </div>
         </div>
         <div class="col-md-8 d-flex justify-content-start align-items-start">
            <div class="card text-white bg-primary mr-3">
               <div class="card-header">Alamat Asal</div>
               <div class="card-body">
                  <p class="card-text">{{$biodata->alamat_asal}}</p>
               </div>
            </div>
            <div class="card text-white bg-info">
               <div class="card-header">Alamat Domisili</div>
               <div class="card-body">
                  <p class="card-text">{{$biodata->alamat_domisili}}</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
