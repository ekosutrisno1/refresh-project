@extends('layouts.app')

@section('judul')
{{$judul}}
@endsection

@section('content')
<div class="card-body w-75 mx-auto">
   <a href="/all" class="btn btn-primary btn-sm mb-3"><i class="fa fa-fw fa-arrow-left"></i> Back to home</a>
   <form action="/update/{{$biodata->id}}" method="POST" enctype="multipart/form-data">
      <input required type="hidden" type="text" name="id" value="{{$biodata->id}}">
      @csrf
      <div class="row">
         <div class="col-sm">
            <div class="form-group">
               <label>Nama Lengkap</label>
               <input required value="{{$biodata->nama_lengkap}}" name="nama_lengkap" id="nama_lengkap" type="text"
                  class="form-control" placeholder="Your nama ...">
            </div>
         </div>
         <div class="col-sm">
            <div class="form-group">
               <label>Email</label>
               <input required value="{{$biodata->email}}" name="email" id="email" type="email" class="form-control"
                  placeholder="Your email...">
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm">
            <div class="form-group">
               <label>Nomor Identitas</label>
               <input required value="{{$biodata->no_identitas}}" name="no_identitas" id="no_identitas" type="number"
                  min="0" class="form-control" placeholder="Your NIK/NIS ...">
            </div>
         </div>
         <div class="col-sm">
            <div class="form-group">
               <label>Umur</label>
               <input required value="{{$biodata->umur}}" name="umur" id="umur" type="number" min="0"
                  class="form-control" placeholder="Your age...">
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm">
            <label class="mr-sm-2" for="gender">Jenis Kelamin</label>
            <select value="{{$biodata->gender}}" name="gender" class="custom-select mr-sm-2" id="gender">
               @if ($biodata->gender==1)
               <option value="1" selected>Laki-laki</option>
               <option value="2">Perempuan</option>
               @else
               <option value="1">Laki-laki</option>
               <option value="2" selected>Perempuan</option>
               @endif
            </select>
         </div>
         <div class="col-sm">
            <div class="form-group">
               <label>No Telepon</label>
               <input required value="{{$biodata->no_telepon}}" name="no_telepon" id="no_telepon" type="number" min="0"
                  class="form-control" placeholder="Your phone number...">
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm">
            <!-- textarea -->
            <div class="form-group">
               <textarea required name="alamat_asal" class="form-control" rows="2"
                  placeholder="Alamat asal ...">{{$biodata->alamat_asal}}</textarea>
            </div>
         </div>
         <div class="col-sm">
            <!-- textarea -->
            <div class="form-group">
               <textarea required name="alamat_domisili" class="form-control" rows="2"
                  placeholder="Alamat domisili ...">{{$biodata->alamat_domisili}}</textarea>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm">
            <!-- textarea -->
            <div class="form-group">
               <textarea required name="posisi_sekarang" class="form-control" rows="2"
                  placeholder="Enter ...">{{$biodata->posisi_sekarang}}</textarea>
            </div>
         </div>
      </div>
      <div class="row float-right">
         <button type="reset" class="btn btn-outline-primary mr-2 float-right">Reset Data</button>
         <button type="submit" class="btn btn-primary mr-2">Send Data</button>
      </div>

   </form>
</div>
@endsection
