@extends('layouts.app')

@section('judul')
{{$judul}}
@endsection

@section('content')
<div class="container">
    <div class="col">
        @if (session('status'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Sukses, </strong> {{session('status')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
    </div>
    <div class="row py-3">
        <h5 class="text-uppercase text-center mx-auto">All Data Collected</h5>
    </div>
    <div class="row">
        <div class="col-md mx-auto">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">Nomor ID</th>
                        <th scope="col">Email</th>
                        <th scope="col">Umur</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($biodata as $i =>$bio)
                    <tr>
                        <th scope="row">{{$i+1}}</th>
                        <td>{{$bio->nama_lengkap}}</td>
                        <td>{{$bio->no_identitas}}</td>
                        <td>{{$bio->email}}</td>
                        <td>{{$bio->umur}} tahun</td>
                        <td>
                            @if ($bio->gender==1)
                            <span>Laki-laki</span>
                            @else
                            <span>Perempuan</span>
                            @endif
                        </td>
                        <td>

                            <a href="/detail/{{$bio->id}}" class="btn btn-sm btn-primary mr-2">
                                Detail <i class="fa fa-fw fa-search"></i>
                            </a>
                            <a href="/edit/{{$bio->id}}" class="btn btn-sm btn-warning mr-2">
                                Edit <i class="fa fa-fw fa-pencil"></i>
                            </a>
                            <button type="button" class="btn btn-outline-danger btn-sm"
                                onclick="deleteBiodata({{$bio->id}})">Hapus
                                <i class="fa fa-fw fa-trash"></i>
                            </button>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <form class="d-none" id="delete-form" action="" method="POST">
                {{-- {{ method_field('DELETE') }} --}}
                @csrf
            </form>
        </div>
    </div>
</div>
@endsection
