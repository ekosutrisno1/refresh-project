<?php

namespace App\Http\Controllers;

use App\Biodata;
use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function detail($biodataId)
    {
        $judul = 'Detail Data';
        $biodata = Biodata::find($biodataId);

        return view("pages.show", ['biodata' => $biodata, 'judul' => $judul]);
    }

    public function all()
    {
        $biodata = Biodata::all();
        $judul = 'Load All Data';
        return view("pages.all", ['biodata' => $biodata, 'judul' => $judul]);
    }

    public function add()
    {
        $judul = 'Form Insert Data';
        return view("pages.add", ['judul' => $judul]);
    }

    public function edit($biodataId)
    {
        $judul = 'Form Update Data';
        $biodata = Biodata::find($biodataId);

        return view("pages.edit", ['biodata' => $biodata, 'judul' => $judul]);
    }

    public function insert(Request $request)
    {

        $biodata = new Biodata([
            'email' => $request->email,
            'nama_lengkap' => $request->nama_lengkap,
            'no_identitas' => $request->no_identitas,
            'umur' => $request->umur,
            'gender' => $request->gender,
            'no_telepon' => $request->no_telepon,
            'alamat_asal' => $request->alamat_asal,
            'alamat_domisili' => $request->alamat_domisili,
            'posisi_sekarang' => $request->posisi_sekarang,
        ]);

        $biodata->save();

        return redirect('/all')->with('status', 'Terimakasih telah memberikan Informasi');

    }

    public function update($biodataId, Request $request)
    {
        $biodataUpdate = Biodata::find($biodataId);

        $biodataUpdate->email = $request->email;
        $biodataUpdate->nama_lengkap = $request->nama_lengkap;
        $biodataUpdate->no_identitas = $request->no_identitas;
        $biodataUpdate->umur = $request->umur;
        $biodataUpdate->gender = $request->gender;
        $biodataUpdate->no_telepon = $request->no_telepon;
        $biodataUpdate->alamat_asal = $request->alamat_asal;
        $biodataUpdate->alamat_domisili = $request->alamat_domisili;
        $biodataUpdate->posisi_sekarang = $request->posisi_sekarang;

        $biodataUpdate->save();

        return redirect('/all')->with('status', 'Data telah diupdate.');
    }

    public function delete($biodataId)
    {
        $biodata = Biodata::find($biodataId);
        $biodata->delete();
        return back()->with('status', 'Data telah berhasil dihapus.');
    }

}
