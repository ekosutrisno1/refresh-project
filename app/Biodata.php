<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $fillable = [
        'email',
        'nama_lengkap',
        'umur',
        'gender',
        'no_identitas',
        'no_telepon',
        'alamat_asal',
        'alamat_domisili',
        'posisi_sekarang',
    ];
}
