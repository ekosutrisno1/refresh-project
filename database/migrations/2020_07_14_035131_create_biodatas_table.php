<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiodatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodatas', function (Blueprint $table) {
            $table->id();
            $table->string('email', 150);
            $table->string('nama_lengkap', 150);
            $table->string('no_identitas', 150);
            $table->integer('umur');
            $table->integer('gender');
            $table->string('no_telepon', 150);
            $table->text('alamat_asal');
            $table->text('alamat_domisili');
            $table->text('posisi_sekarang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodatas');
    }
}
