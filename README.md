# Refresh Project

Refresh Project PHP @ekosutrisno1 Eko Sutrisno
database menggunakan postgresql

```bash
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=refresh_db
DB_USERNAME=<your username>
DB_PASSWORD=<your password>
```

Atau bisa menggunakan mysql dengan phpMyAdmin

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=refresh_db
DB_USERNAME=<your username>
DB_PASSWORD=<your password>
```
