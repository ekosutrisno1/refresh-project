<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', 'BiodataController@index');
Route::get('/all', 'BiodataController@all');
Route::get('/add', 'BiodataController@add');
Route::get('/edit/{id}', 'BiodataController@edit');
Route::get('/detail/{id}', 'BiodataController@detail');

Route::post('/insert', 'BiodataController@insert');
Route::post('/update/{id}', 'BiodataController@update');
Route::post('/delete/{id}', 'BiodataController@delete');
